variable "service_name" {
  description = "Service/app name kubernetes"
  type        = string
}

variable "namespace" {
  description = "Service/app namespace kubernetes"
  type        = string
}

variable "image" {
  description = "Image container"
  type        = string
}

variable "env_secret_var" {
  description = "Secret var"
  type        = string
}

variable "ports" {
  description = "ports expose service"
  type = object({
    port          = string
    target_port   = number
  })

  default = {
    port          = 0
    target_port   = 0
  }
}

variable "hpa" {
  description = "hpa"
  type = object({
    max_replica         = number
    min_replica         = number
    cpu_threshold       = number
  })

  default = {
    max_replica   = 1
    min_replica   = 1
    cpu_threshold = 50
  }
}

variable "limits" {
  description = "limit"
  type = object({
    cpu          = string
    memory       = string
  })

  default = {
    cpu          = ""
    memory       = ""
  }
}


variable "requests" {
  description = "requests"
  type = object({
    cpu          = string
    memory       = string
  })

  default = {
    cpu          = ""
    memory       = ""
  }
}
