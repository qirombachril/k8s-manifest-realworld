provider "kubernetes" {
  config_path    = "/root/.kube/config"
}

resource "kubernetes_deployment" "example" {
  metadata {
    name = "${var.service_name}-deploy"
    namespace = var.namespace 
    labels = {
      app = "${var.service_name}-app"
    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "${var.service_name}-app"
      }
    }

    template {
      metadata {
        labels = {
          app = "${var.service_name}-app"
        }
      }

      spec {
        container {
          image = "${var.image}"
          name  = "${var.service_name}-container"
          env_from  {
            secret_ref  {
               name = var.env_secret_var
             }
          }
          resources {
            limits = {
              cpu    = var.limits.cpu
              memory = var.limits.memory
            }
            requests = {
              cpu    = var.requests.cpu
              memory = var.requests.memory
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "example" {
  metadata {
    namespace = var.namespace 
    name = "${var.service_name}-service"
  }
  spec {
    selector = {
      app = kubernetes_deployment.example.metadata.0.labels.app
    }
    port {
      port        = var.ports.port
      target_port = var.ports.target_port
    }

    type = "NodePort"
  }
}

resource "kubernetes_horizontal_pod_autoscaler" "example" {
  metadata {
    name = "${var.service_name}-hpa"
    namespace = var.namespace
  }

  spec {
    max_replicas = var.hpa.max_replica
    min_replicas = var.hpa.min_replica

    scale_target_ref {
      kind = "Deployment"
      name = "${var.service_name}-deploy"
      api_version = "apps/v1"
    }
    target_cpu_utilization_percentage = var.hpa.cpu_threshold

  }
}


