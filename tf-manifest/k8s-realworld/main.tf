module "k8s_manifest" {
  source = "../modules/"
  service_name = "realworld"
  namespace = "realworld"
  image = "bachrilq/realworld-service:501debb2"
  env_secret_var = "app-secret"
  limits = {
    cpu = "1"
    memory = "500Mi"
  }
  requests = {
    cpu = "250m"
    memory = "100Mi"
  }
  ports = {
    port = "3000"
    target_port = "3000"
  }
  hpa = {
    max_replica = 2
    min_replica = 1
    cpu_threshold = 70
  }
}
