terraform {
    backend "s3" {
        bucket = "tf-state"                  
endpoints = {
            s3 = "http://127.0.0.1:9000"   # Minio endpoint
        }
        prefix = "tf-manifest/k8s-manifest-realworld/k8s-realworld"
        key = "realworld.tfstate"
        region = "main"                     
skip_requesting_account_id = true
        skip_credentials_validation = true  
        skip_metadata_api_check = true
        skip_region_validation = true
        force_path_style = true             
    }

}

